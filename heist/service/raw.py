# Execute a binary directly without running through an init program


def start(hub):
    ...


def stop(hub):
    ...


def restart(hub):
    ...
